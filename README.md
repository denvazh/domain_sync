Gateway domain sync
---

Syncronize public ip address for domain registered with *reg.ru* service.

Version: 0.0.1

Security note
---

> Important!
> Because this solution involved in creating and storing account information in plain text encryption/decryption) it is important to limit access to the server and make sure nobody except root/backup user has access priveledges to the information stored in config.yml file.

## How to configure

By default, script sync_domain.rb expects configuration file **config.yml**
in the same directory.

Using option **-c** or **--conf** it is possible to supply this file from any other directory in the file system.

  example: ruby sync_domain.rb --conf $HOME/config.yml


### Structure of config.yml

Configuration file is expected to have the following structure:

	account: '%hostname%'
    username: '%username%'
    password: '%password%'
    lang: 'ru'
	domain:
    	name: 'example.com'
    	subdomain: 'gate1'

