#!/usr/bin/env ruby

require 'yaml'
require 'optparse'
require './lib/public_domain'

require 'bundler/setup'
require 'reg_api2'

# Load account configuration
options = {}
opt_parser =OptionParser.new do |opts|
  opts.banner = "Usage: sync_domain.rb [options]"

  options[:config] = nil
    opts.on("-c", "--conf [CONFIGURATION]", String,
      "Configuration file to use [Default: config.yml]") do |c|
    options[:config] = c
  end

  opts.on("-h", "--help", "Show help") do |h|
    options[:help] = h
    puts opt_parser
    exit
  end

  opts.on("-v", "--version", "Show current version") do |v|
    options[:version] = v
  end
end

begin
  opt_parser.parse!

  if (options[:version])
    puts CustomDomain::VERSION
    exit
  end

rescue OptionParser::InvalidOption, OptionParser::MissingArgument
  puts $!.to_s
  puts opt_parser
  exit
end

# [MAIN]

# Gateway 
gateway = CustomDomain::Gateway.new(options[:config])

# Query regapi
RegApi2.username  =gateway.username
RegApi2.password  =gateway.password
RegApi2.lang      =gateway.lang

begin
  RegApi2.zone.add_alias(domains: [ { dname: "#{gateway.domain}" } ],
  subdomain: "#{gateway.subdomain}", ipaddr: "#{gateway.public_ip}")
rescue
  puts $!.to_s
  exit
end

# [MAIN][END]
