require 'uri'
require 'net/http'
require 'json'

module CustomDomain
  VERSION='0.0.1'

  class Gateway
    DEFAULT_CONFIG='config.yml'

    attr :public_ip
    attr :subnet
    attr :private_ip

    attr :username
    attr :password
    attr :lang
    attr :domain
    attr :subdomain

    def initialize(configFile=nil)
      config = readconf(configFile)

      # Read configuration file
      if (verifyconf(config))
          @username   =config['account']['username']
          @password   =config['account']['password']
          @lang       =config['account']['lang']
          @domain     =config['domain']['name']
          @subdomain  =config['domain']['subdomain']
      end

      # Query public ip of gateway
      query_service = PublicQuery.new
      @public_ip = query_service.get_public_address
    end

    def readconf(configFile=nil)
      file = configFile ? configFile : DEFAULT_CONFIG
      begin
        conf = YAML::load_file(file)
        return conf
      rescue Errno::ENOENT
        puts "[Error] " + $!.to_s
        exit
      end
    end

    def verifyconf(yml)
      expected = ['account', 'domain']

      # check if all keys was set properly
      valid_keys = expected.all? { |e| yml.keys.include?(e) }

      # check if all values for keys is at least not null
      valid_values = yml.all? { |k,v| v.nil? || v.empty? || v.size == 0 }

      return valid_keys || valid_values ? true : false
    end

    class PublicQuery
      JSONIP='http://jsonip.com'

      attr :public_addr
      
      def initialize()
        uri = URI(JSONIP)

        begin
          res         =Net::HTTP.get(uri)
          res_json    =JSON.parse(res)
          @public_addr  =res_json['ip']
        rescue
          puts $!.to_s
        end
      end

      def get_public_address
        return @public_addr
      end

      private :public_addr
    end # class PublicQuery
  end # class Gateway
end # module CustomDomain
